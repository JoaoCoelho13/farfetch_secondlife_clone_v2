package com.example.luxclusif.model;

public interface Model {

    Long getId();

    void setId(Long id);
}
