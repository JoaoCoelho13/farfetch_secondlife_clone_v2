package com.example.luxclusif.model.bags;

import com.example.luxclusif.model.Model;

import javax.persistence.*;

@Entity
@Table(name= "extras")
public class Extras implements Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Integer version;

    @OneToOne
    private Bag bag;

    private Integer box;
    private Integer authenticityCard;
    private Integer shoulderStrap;


    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Bag getBag() {
        return bag;
    }

    public void setBag(Bag bag) {
        this.bag = bag;
    }

    public Integer getBox() {
        return box;
    }

    public void setBox(Integer box) {
        this.box = box;
    }

    public Integer getAuthenticityCard() {
        return authenticityCard;
    }

    public void setAuthenticityCard(Integer authenticityCard) {
        this.authenticityCard = authenticityCard;
    }

    public Integer getShoulderStrap() {
        return shoulderStrap;
    }

    public void setShoulderStrap(Integer shoulderStrap) {
        this.shoulderStrap = shoulderStrap;
    }

    @Override
    public String toString() {
        return "box : " + box
                + "\nauthenticityCard : " + authenticityCard
                + "\nshoulderStrap : " + shoulderStrap;
    }
}
