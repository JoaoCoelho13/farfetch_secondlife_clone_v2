package com.example.luxclusif.model.bags;

import com.example.luxclusif.model.Case;
import com.example.luxclusif.model.Customer;
import com.example.luxclusif.model.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "Bag")
public class Bag implements Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String currentCondition;
    private String brand;
    private String size;
    private Long extrasId;

    @OneToOne(mappedBy = "bag", cascade = {CascadeType.ALL}, orphanRemoval = true)
    private Extras extras;

    @JsonIgnore
    @OneToOne
    private Case aCase;

    @Version
    private Integer version;


    public Extras getExtras() {
        return extras;
    }

    public void setExtras(Extras extras) {
        this.extras = extras;
    }

    public Case getaCase() {
        return aCase;
    }

    public void setaCase(Case aCase) {
        this.aCase = aCase;
    }

    public Long getExtrasId() {
        return extrasId;
    }

    public void setExtrasId(Long extrasId) {
        this.extrasId = extrasId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getCurrentCondition() {
        return currentCondition;
    }

    public void setCurrentCondition(String currentCondition) {
        this.currentCondition = currentCondition;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
