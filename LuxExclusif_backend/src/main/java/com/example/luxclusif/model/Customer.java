package com.example.luxclusif.model;

import com.example.luxclusif.model.bags.Bag;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "customer")
public class Customer implements Model{

    @OneToMany(
            // propagate changes on customer entity to account entities
            cascade = {CascadeType.ALL},

            // make sure to remove bags still on sale if unlinked from customer
            orphanRemoval = true,

            // user customer foreign key on account table to establish
            // the many-to-one relationship instead of a join table
            mappedBy = "customer",

            // fetch bags from database together with user
            fetch = FetchType.EAGER
    )
    private final List<Case> caseList = new ArrayList<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Version
    private Integer version;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private Double balance = 0.0;


    public void addToCaseList(Case aCase) {
        this.caseList.add(aCase);
        aCase.setCustomer(this);
    }

    public void removeCase(Case aCase) {
        caseList.remove(aCase);
        aCase.setCustomer(null);
    }

    public void addCredit(Double balance) {
        this.balance += balance;
    }

    public boolean removeCredit(Double balance) {
        if (this.balance >= balance) {
            this.balance -= balance;
            return true;
        } else {
            return false;
        }
    }

    public List<Case> getCaseList() {
        return caseList;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }
}
