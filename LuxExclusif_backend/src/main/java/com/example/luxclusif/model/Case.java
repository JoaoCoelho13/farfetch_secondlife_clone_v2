package com.example.luxclusif.model;


import com.example.luxclusif.model.bags.Bag;
import com.example.luxclusif.model.bags.Extras;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "case_tbl")
public class Case implements Model{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Integer version;

    private Long bagId;

    @OneToOne(mappedBy = "aCase", cascade = {CascadeType.ALL}, orphanRemoval = true)
    private Bag bag;

    @JsonIgnore
    @ManyToOne
    private Customer customer;

    public Case() {

    }

    public Case(Long bagId){
        this.bagId = bagId;
    }


    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long case_id) {
        this.id = case_id;
    }

    public Bag getBag() {
        return bag;
    }

    public void setBag(Bag bag) {
        this.bag = bag;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Long getBagId() {
        return bagId;
    }

    public void setBagId(Long bagId) {
        this.bagId = bagId;
    }

}
