package com.example.luxclusif.converters;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Component
public class ImageHandler {

    public File imageSaver(MultipartFile file, Long caseId) {
        File newFile = null;

        //For Windows \\
        //String IMAGE_BASE_PATH = "E:\\uploads\\SecondLife\\resources\\bag_images\\";

        //For linux /
        String IMAGE_DOWNLOAD_BASE_PATH = "uploads/SecondLife/resources/bag_images/";
        File imageFolder = new File(IMAGE_DOWNLOAD_BASE_PATH + caseId);

        if (imageFolder.exists() || imageFolder.mkdirs()) {

            newFile = new File(imageFolder.getAbsolutePath() + "/" + file.getOriginalFilename());
            try {
                file.transferTo(newFile);
            } catch (IOException e) {
                return null;
            }
        }

        return newFile;
    }


}
