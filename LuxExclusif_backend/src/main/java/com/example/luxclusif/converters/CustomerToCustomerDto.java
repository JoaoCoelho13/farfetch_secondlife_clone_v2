package com.example.luxclusif.converters;

import com.example.luxclusif.command.CustomerDto;
import com.example.luxclusif.model.Customer;
import org.springframework.stereotype.Component;

@Component
public class CustomerToCustomerDto extends AbstractConverter<Customer, CustomerDto> {

    public CustomerDto convert(Customer customer) {

        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(customer.getId());
        customerDto.setFirstName(customer.getFirstName());
        customerDto.setLastName(customer.getLastName());
        customerDto.setEmail(customer.getEmail());
        customerDto.setPhone(customer.getPhone());
        customerDto.setBalance(customer.getBalance().toString());
        //customerDto.setBagNumber(customer.getBagsList().size());

        return customerDto;
    }
}
