package com.example.luxclusif.converters;

import com.example.luxclusif.command.BagDto;
import com.example.luxclusif.model.bags.Bag;
import com.example.luxclusif.service.BagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class BagDtoToBag implements Converter<BagDto, Bag> {

    private BagService bagService;


    @Autowired
    public void setBagService(BagService bagService) {
        this.bagService = bagService;
    }

    @Override
    public Bag convert(BagDto bagDto) {
        Bag bag = (bagDto.getBagId() != null ? bagService.getBag(bagDto.getBagId()) : new Bag());

        bag.setBrand(bagDto.getBrand());
        bag.setSize(bagDto.getSize());
        bag.setCurrentCondition(bagDto.getCondition());

        System.out.println(bagDto.getExtras());

        if(bagDto.getExtras().getAuthenticityCard() != 0 || bagDto.getExtras().getBox() != 0
                || bagDto.getExtras().getShoulderStrap() != 0) {

            bag.setExtras(bagDto.getExtras());
        }

        return bag;
    }

}
