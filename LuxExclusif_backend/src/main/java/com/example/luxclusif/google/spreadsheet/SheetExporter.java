package com.example.luxclusif.google.spreadsheet;

import com.example.luxclusif.google.Exporter;
import com.example.luxclusif.google.authentication.GoogleAuthentication;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.AppendValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Component
public class SheetExporter implements Exporter<String> {

    private static final String TOKENS_DIRECTORY_PATH = "tokens";

    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */

    private static final String spreadsheetId = "162cdQFVC0l1NagVb9tBUvAH4tOVs5TXSkh6944girvE";

    private Sheets service;
    private GoogleAuthentication googleAuthentication;

    @Autowired
    public void setGoogleAuthentication(GoogleAuthentication googleAuthentication) {
        this.googleAuthentication = googleAuthentication;
    }


    @Override
    public void export(String table, Long id) throws Exception {
        authenticate();
        exportData(table);
    }

    @Override
    public void authenticate() {
        try {
            googleAuthentication.getCredentials(this.getClass());

            service = new Sheets.Builder(googleAuthentication.HTTP_TRANSPORT, googleAuthentication.JSON_FACTORY, googleAuthentication.getCredentials(this.getClass()))
                    .setApplicationName(googleAuthentication.APPLICATION_NAME)
                    .build();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private Statement accessDatabase() {

        String jdbcURL = "jdbc:mysql://localhost:3306/secondLife_clone?serverTimezone=UTC&useSSL=false";

        //For Windows:
       /* String username = "luxclusif";
        String password = "!Password123";*/

        String username = "root";
        String password = "";
        Statement statement = null;

        try {
            Connection connection = DriverManager.getConnection(jdbcURL, username, password);

            statement = connection.createStatement();

        } catch (SQLException e) {
            e.printStackTrace();

        }

        return statement;

    }

    private void exportData(String table) throws Exception {

        String sql = "SELECT * FROM ".concat(table).concat(" ORDER BY id DESC LIMIT 1");

        ResultSet resultSet = accessDatabase().executeQuery(sql);

        ResultSetMetaData metaData = resultSet.getMetaData();

        int colNumber = metaData.getColumnCount();
        List<Object> list = new LinkedList<>();

        while (resultSet.next()) {
            for (int i = 1; i <= colNumber; i++) {
                list.add(resultSet.getObject(i));
            }

            ValueRange appendBody = new ValueRange()
                    .setValues(Collections.singletonList(
                            list
                    ));

            AppendValuesResponse appendResult = service.spreadsheets().values()
                    .append(spreadsheetId, table, appendBody)
                    .setValueInputOption("USER_ENTERED")
                    .setInsertDataOption("INSERT_ROWS")
                    .setIncludeValuesInResponse(true)
                    .execute();

            list.clear();
        }
    }
}
