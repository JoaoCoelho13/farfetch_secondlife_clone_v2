package com.example.luxclusif.google.images;

import com.example.luxclusif.google.Exporter;
import com.example.luxclusif.google.authentication.GoogleAuthentication;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.http.FileContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.FileList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.lang.reflect.Field;
import java.util.Collections;


@Component
public class DriveExporter implements Exporter<File> {

    private GoogleAuthentication googleAuthentication;
    private Drive service;

    @Autowired
    public void setGoogleAuthentication(GoogleAuthentication googleAuthentication) {
        this.googleAuthentication = googleAuthentication;
    }

    @Override
    public void authenticate() {
        try {

            service = new Drive.Builder(googleAuthentication.HTTP_TRANSPORT, googleAuthentication.JSON_FACTORY, googleAuthentication.getCredentials(this.getClass()))
                    .setApplicationName(googleAuthentication.APPLICATION_NAME)
                    .build();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void export(java.io.File model, Long id) {
        authenticate();
        generateData(model, id);
    }

    private void generateData(java.io.File file, Long id) {
        String folderId = searchFolder(id);
        System.out.println("Folder id: " + folderId);

        if (folderId == null) {

            folderId = createFolder(id);
        }


        //create File
        com.google.api.services.drive.model.File fileMetadata = new com.google.api.services.drive.model.File();
        fileMetadata.setName(file.getName());
        fileMetadata.setParents(Collections.singletonList(folderId));
        FileContent mediaContent = new FileContent("image/jpeg", file);
        try {
            Drive.Files.Create create = service.files().create(fileMetadata, mediaContent)
                    .setFields("id, parents");
            MediaHttpUploader uploader = create.getMediaHttpUploader();
            uploader.setDirectUploadEnabled(true);



            com.google.api.services.drive.model.File imageCreatedOnDrive = create.execute();
            System.out.println(imageCreatedOnDrive.getId());


        } catch (IOException e) {
            System.out.println("Error creating file!");
            e.printStackTrace();
        }

    }

    private String createFolder(Long id) {
        String folderId;
        com.google.api.services.drive.model.File folderMetadata = new com.google.api.services.drive.model.File();
        folderMetadata.setName("case_" + id);
        folderMetadata.setMimeType("application/vnd.google-apps.folder");

        try {
            Drive.Files.Create createFolder = service.files().create(folderMetadata);
            createFolder.execute();
            folderId = searchFolder(id);

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error creating folder!");
            return null;
        }

        System.out.println("Folder created with id: " + folderId);
        return folderId;
    }

    private String searchFolder(Long id) {

        FileList result = null;

        try {
            //Search for folder in googleDrive
            String pageToken;
            do {
                //Searches for folder types, with name equal to the case_id and that are not in the trash bin!
                result = service.files().list()
                        .setQ("mimeType='application/vnd.google-apps.folder' and name='case_" + id + "' and trashed = false")
                        .setSpaces("drive")
                        .setFields("nextPageToken, files(id, name)")
                        .execute();
                pageToken = result.getNextPageToken();
            } while (pageToken != null);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (result == null || result.getFiles().isEmpty()) {
            System.out.println("Folder not found. Returned null");
            return null;
        }
        System.out.println("Folder found!");
        return result.getFiles().get(0).getId();
    }

    public void searchImage() {

        authenticate();

        String pageToken = null;

        try {
            do {
                FileList result = service.files().list()
                        .setQ("mimeType='image/jpeg' and name contains 'winky'")
                        .setSpaces("drive")
                        .setFields("nextPageToken, files(id, name)")
                        .setPageToken(pageToken)
                        .execute();

                for (com.google.api.services.drive.model.File file : result.getFiles()) {
                    System.out.printf("Found file: %s (%s)\n",
                            file.getName(), file.getId());
                }
                pageToken = result.getNextPageToken();
            } while (pageToken != null);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    public void searchImagea() {

        authenticate();

        try {

            String result = service.files().list().getSpaces();

            System.out.println(result);

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }



    public void testImageDownloaderGdrive() {
        authenticate();

        String IMAGE_DOWNLOAD_BASE_PATH = "uploads/Farfetch/resources/bag_images/23";

        String fileID = "15bzskfwNqPYT9UeXaspjAX2oQaB26gh4";

        File test = new File(IMAGE_DOWNLOAD_BASE_PATH);

        if (test.exists() || test.mkdirs()) {
            File newFile = new File(test.getAbsolutePath() + "/ze.jpg");

            try {
                OutputStream outputStream = new FileOutputStream(newFile);

                System.out.println("outputstream created");

               service.files().get(fileID).executeMediaAndDownloadTo(outputStream);
               System.out.println("file downloaded to outputstream at location given woooowoo");

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
