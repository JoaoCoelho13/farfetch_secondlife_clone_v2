package com.example.luxclusif.google;

public interface Exporter<T> {

    void authenticate();

    void export(T model, Long id) throws Exception;
}
