package com.example.luxclusif.repositories;

import com.example.luxclusif.model.Customer;

public interface CustomerDao extends Dao<Customer>{

    Customer findByEmail(String email);
}
