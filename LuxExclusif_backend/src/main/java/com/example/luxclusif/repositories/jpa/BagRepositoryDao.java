package com.example.luxclusif.repositories.jpa;

import com.example.luxclusif.model.bags.Bag;
import com.example.luxclusif.repositories.BagDao;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.Query;

@Repository
public class BagRepositoryDao extends GenericDao<Bag> implements BagDao {

    public BagRepositoryDao() {
        super(Bag.class);
    }


    @Override
    public void updateBag(Long bagId, Long extrasId) {

        try {
            Query query = em.createQuery("UPDATE Bag SET extrasId = :extrasId WHERE id = :bagId")
                    .setParameter("extrasId", extrasId)
                    .setParameter("bagId", bagId);

            int result = query.executeUpdate();


            super.findById(bagId).setExtrasId(extrasId);

        } catch (NoResultException e) {
            System.out.println(e.getMessage());
        }


    }

    @Override
    public void updateBagWithExtrasNull(Long bagId) {

        try {
            Query query = em.createQuery("UPDATE Bag SET extrasId = 0 WHERE id = :bagId")
                    .setParameter("bagId", bagId);

            int result = query.executeUpdate();


        } catch (NoResultException e) {
            System.out.println(e.getMessage());
        }


    }
}
