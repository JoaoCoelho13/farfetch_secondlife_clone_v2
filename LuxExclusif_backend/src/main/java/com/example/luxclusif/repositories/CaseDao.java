package com.example.luxclusif.repositories;

import com.example.luxclusif.model.Case;

public interface CaseDao extends Dao<Case> {

    void updateCase(Long caseId, Long bagId);

}
