package com.example.luxclusif.repositories;


import com.example.luxclusif.model.Model;

import java.util.List;

public interface Dao<T extends Model> {

    List<T> findAll();

    T findById(Long id);

    T save(T modelObject);

    void delete(Long id);

}
