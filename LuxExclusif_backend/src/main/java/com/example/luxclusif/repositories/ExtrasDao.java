package com.example.luxclusif.repositories;

import com.example.luxclusif.model.bags.Extras;

public interface ExtrasDao extends Dao<Extras> {

    void updateExtras(Long extrasId, Long bagId);
}
