package com.example.luxclusif.repositories.jpa;

import com.example.luxclusif.model.bags.Extras;
import com.example.luxclusif.repositories.ExtrasDao;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.Query;

@Repository
public class ExtrasRepositoryDao extends GenericDao<Extras> implements ExtrasDao {

    public ExtrasRepositoryDao() {
        super(Extras.class);
    }


    @Override
    public void updateExtras(Long extrasId, Long bagId) {

        try {
            Query query = em.createQuery("UPDATE Extras SET bag_id = :bagId WHERE id = :extrasId")
                    .setParameter("extrasId", extrasId)
                    .setParameter("bagId", bagId);

            int result = query.executeUpdate();

        } catch (NoResultException e) {
            System.out.println(e.getMessage());
        }
    }
}
