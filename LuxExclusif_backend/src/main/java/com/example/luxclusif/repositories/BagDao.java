package com.example.luxclusif.repositories;

import com.example.luxclusif.model.bags.Bag;

public interface BagDao extends Dao<Bag>{

    void updateBag (Long bagId, Long caseId);

    void updateBagWithExtrasNull (Long bagId);
}
