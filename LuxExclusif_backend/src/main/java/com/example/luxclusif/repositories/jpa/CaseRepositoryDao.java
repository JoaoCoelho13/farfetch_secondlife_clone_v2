package com.example.luxclusif.repositories.jpa;

import com.example.luxclusif.model.Case;
import com.example.luxclusif.repositories.CaseDao;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.Query;

@Repository
public class CaseRepositoryDao extends GenericDao<Case> implements CaseDao {

    public CaseRepositoryDao() {
        super(Case.class);
    }

    @Override
    public void updateCase(Long caseId, Long bagId) {

        try {
            Query query = em.createQuery("UPDATE Case SET bagId = :bagId WHERE id = :caseId")
                    .setParameter("caseId", caseId)
                    .setParameter("bagId", bagId);

            int result = query.executeUpdate();

        } catch (NoResultException e) {
            System.out.println(e.getMessage());
        }
    }
}
