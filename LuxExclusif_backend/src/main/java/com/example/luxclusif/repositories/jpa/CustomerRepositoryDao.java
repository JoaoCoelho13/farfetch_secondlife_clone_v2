package com.example.luxclusif.repositories.jpa;

import com.example.luxclusif.model.Customer;
import com.example.luxclusif.repositories.CustomerDao;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class CustomerRepositoryDao extends GenericDao<Customer> implements CustomerDao {

    public CustomerRepositoryDao() {
        super(Customer.class);
    }

    @Override
    public Customer findByEmail(String email) {

        Customer customer;
        try {
            customer = em.createQuery("SELECT c from Customer c WHERE c.email = :custEmail", Customer.class)
                    .setParameter("custEmail", email)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        return customer;
    }
}
