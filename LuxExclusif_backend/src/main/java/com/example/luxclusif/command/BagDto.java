package com.example.luxclusif.command;

import com.example.luxclusif.model.bags.Extras;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class BagDto {

    private Long bagId;

    private Long customerId;

    @NotNull(message = "Brand is mandatory")
    @NotBlank(message = "Brand is mandatory")
    private String brand;

    @NotNull(message = "Condition is necessary")
    @NotBlank(message = "Condition is necessary")
    private String condition;

    @NotNull(message = "Size is necessary")
    @NotBlank(message = "Size is necessary")
    private String size;

    private String imagesUrl;

    private Extras extras;


    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getBagId() {
        return bagId;
    }

    public void setBagId(Long bagId) {
        this.bagId = bagId;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getImagesUrl() {
        return imagesUrl;
    }

    public void setImagesUrl(String imagesUrl) {
        this.imagesUrl = imagesUrl;
    }

    public Extras getExtras() {
        return extras;
    }

    public void setExtras(Extras extras) {
        this.extras = extras;
    }

}
