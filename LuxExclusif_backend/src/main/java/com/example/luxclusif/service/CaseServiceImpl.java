package com.example.luxclusif.service;

import com.example.luxclusif.model.Case;
import com.example.luxclusif.model.bags.Bag;
import com.example.luxclusif.repositories.CaseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class CaseServiceImpl implements CaseService {

    @Autowired
    private CaseDao caseDao;


    @Override
    public Case getCase(Long id) {
        return caseDao.findById(id);
    }

    @Override
    @Transactional
    public Case createCase(Bag savedBag) {

        Case aCase = new Case();


        aCase = caseDao.save(aCase);

        return aCase;
    }



    @Transactional
    @Override
    public void updateCase(Long caseId, Long bagId) {
        caseDao.updateCase(caseId, bagId);
    }

}
