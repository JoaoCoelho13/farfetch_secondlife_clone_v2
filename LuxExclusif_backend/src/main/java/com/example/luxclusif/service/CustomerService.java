package com.example.luxclusif.service;

import com.example.luxclusif.exception.AssociationExistsException;
import com.example.luxclusif.exception.CustomerNotFoundException;
import com.example.luxclusif.exception.NotEnoughBalanceException;
import com.example.luxclusif.model.Customer;
import com.example.luxclusif.model.bags.Bag;

import java.util.List;

public interface CustomerService {

    Customer get(Long id);

    Customer getByEmail(String email);

    List<Bag> getCustomerBags(Long id);

    List<Customer> list();

    Double getCredit(Long id);

    void deposit(Long id, Double balance) throws CustomerNotFoundException;

    void withdraw(Long id, Double balance) throws CustomerNotFoundException, NotEnoughBalanceException;

    Customer save(Customer customer);

    void delete(Long bid) throws CustomerNotFoundException, AssociationExistsException;

}
