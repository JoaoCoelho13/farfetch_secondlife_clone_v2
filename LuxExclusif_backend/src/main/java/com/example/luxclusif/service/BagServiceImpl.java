package com.example.luxclusif.service;

import com.example.luxclusif.exception.BagNotFoundException;
import com.example.luxclusif.exception.CustomerNotFoundException;
import com.example.luxclusif.model.Case;
import com.example.luxclusif.model.Customer;
import com.example.luxclusif.model.bags.Bag;
import com.example.luxclusif.model.bags.Extras;
import com.example.luxclusif.repositories.BagDao;
import com.example.luxclusif.repositories.CustomerDao;
import com.example.luxclusif.repositories.ExtrasDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;


@Service
public class BagServiceImpl implements BagService{


    @Autowired
    private BagDao bagDao;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private ExtrasDao extrasDao;

    @Override
    public void addExtras(Long id, Extras extras) {
        bagDao.findById(id).setExtras(extras);
    }

    @Override
    public Bag getBag(Long bid) {
        return bagDao.findById(bid);
    }

    @Override
    @Transactional
    public Bag save(Long customerId, Case aCase, Bag bag) throws CustomerNotFoundException {
        Customer customer = Optional.ofNullable(customerDao.findById(customerId))
                .orElseThrow(CustomerNotFoundException::new);

        aCase.setBag(bag);
        bag.setaCase(aCase);

        if (!customer.getCaseList().contains(aCase)) {
            customer.addToCaseList(aCase);
            customerDao.save(customer);
        }

        return customer.getCaseList().get(customer.getCaseList().size() - 1).getBag();
    }


    @Override
    @Transactional
    public void delete(Long cid, Long bid) throws CustomerNotFoundException, BagNotFoundException {
        Customer customer = Optional.ofNullable(customerDao.findById(cid))
                .orElseThrow(CustomerNotFoundException::new);

        Bag bag = Optional.ofNullable(bagDao.findById(bid))
                .orElseThrow(BagNotFoundException::new);

        //ATENCAOOOOOOOOOOOOOOOOOOOOOOOO
        //ATENCAOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
        /*if (!bag.getCustomer().getId().equals(cid)) {
            throw new BagNotFoundException();
        }

        customer.removeBag(bag);*/
        customerDao.save(customer);
    }

    @Override
    @Transactional
    public void updateBag(Long bagId, Long extrasId) {
        bagDao.updateBag(bagId, extrasId);
        extrasDao.updateExtras(extrasId, bagId);
    }

    @Override
    @Transactional
    public void updateBagWithExtrasNull(Long bagId) {
        bagDao.updateBagWithExtrasNull(bagId);
    }
}
