package com.example.luxclusif.service;

import com.example.luxclusif.model.Case;
import com.example.luxclusif.model.bags.Bag;

public interface CaseService {

    Case getCase(Long id);

    Case createCase(Bag savedBag);

    void updateCase(Long caseId, Long bagId);

}
