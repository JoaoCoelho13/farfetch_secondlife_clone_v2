package com.example.luxclusif.service;

import com.example.luxclusif.exception.BagNotFoundException;
import com.example.luxclusif.exception.CustomerNotFoundException;
import com.example.luxclusif.model.Case;
import com.example.luxclusif.model.bags.Bag;
import com.example.luxclusif.model.bags.Extras;

public interface BagService {

    void addExtras(Long bagId, Extras extras);

    Bag getBag(Long bagId);

    Bag save(Long customerId, Case aCase, Bag bag) throws CustomerNotFoundException;

    void delete(Long cid, Long bagId) throws CustomerNotFoundException, BagNotFoundException;

    void updateBag(Long bagId, Long extrasId);

    void updateBagWithExtrasNull (Long bagId);
}
