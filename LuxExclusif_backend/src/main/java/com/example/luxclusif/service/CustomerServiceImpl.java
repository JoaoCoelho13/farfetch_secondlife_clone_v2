package com.example.luxclusif.service;

import com.example.luxclusif.exception.AssociationExistsException;
import com.example.luxclusif.exception.CustomerNotFoundException;
import com.example.luxclusif.exception.NotEnoughBalanceException;
import com.example.luxclusif.model.Customer;
import com.example.luxclusif.model.bags.Bag;
import com.example.luxclusif.repositories.CustomerDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService{

    @Autowired
    private CustomerDao customerDao;


    @Override
    public Customer get(Long id) {
        return customerDao.findById(id);
    }

    @Override
    public Customer getByEmail(String email) {
        return customerDao.findByEmail(email);
    }

    @Override
    public List<Bag> getCustomerBags(Long id) {
        return null;
    }

    @Override
    public List<Customer> list() {
        return null;
    }

    @Override
    public Double getCredit(Long id) {
        return null;
    }

    @Override
    public void deposit(Long id, Double balance) throws CustomerNotFoundException {

    }

    @Override
    public void withdraw(Long id, Double balance) throws CustomerNotFoundException, NotEnoughBalanceException {

    }

    @Override
    @Transactional
    public Customer save(Customer customer) {
        return customerDao.save(customer);
    }

    @Override
    public void delete(Long bid) throws CustomerNotFoundException, AssociationExistsException {

    }
}
