package com.example.luxclusif.exception;

public class LuxclusifException extends Exception {

    public LuxclusifException(String message) {
        super(message);
    }
}
