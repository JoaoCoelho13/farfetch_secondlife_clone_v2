package com.example.luxclusif.exception;

import com.example.luxclusif.errors.ErrorMessage;

public class BagNotFoundException extends LuxclusifException{

    public BagNotFoundException() {
        super(ErrorMessage.BAG_NOT_FOUND);
    }
}
