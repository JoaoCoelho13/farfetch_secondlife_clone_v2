package com.example.luxclusif.exception;

import com.example.luxclusif.errors.ErrorMessage;

public class NotEnoughBalanceException extends LuxclusifException{

    public NotEnoughBalanceException() {
        super(ErrorMessage.NOT_ENOUGH_BALANCE);
    }
}
