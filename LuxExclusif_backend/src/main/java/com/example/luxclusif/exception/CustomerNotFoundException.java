package com.example.luxclusif.exception;

import com.example.luxclusif.errors.ErrorMessage;

public class CustomerNotFoundException extends LuxclusifException {

    public CustomerNotFoundException() {
        super(ErrorMessage.CUSTOMER_NOT_FOUND);
    }
}
