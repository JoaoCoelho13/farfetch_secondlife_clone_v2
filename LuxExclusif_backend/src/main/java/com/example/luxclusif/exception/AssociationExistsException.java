package com.example.luxclusif.exception;

import com.example.luxclusif.errors.ErrorMessage;

public class AssociationExistsException extends LuxclusifException{

    public AssociationExistsException(String message) {
        super(ErrorMessage.ASSOCIATION_EXISTS);
    }
}
