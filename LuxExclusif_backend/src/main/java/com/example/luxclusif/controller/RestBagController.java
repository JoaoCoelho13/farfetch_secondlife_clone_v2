package com.example.luxclusif.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.security.GeneralSecurityException;

import com.example.luxclusif.command.BagDto;
import com.example.luxclusif.converters.BagDtoToBag;
import com.example.luxclusif.converters.ImageHandler;
import com.example.luxclusif.exception.CustomerNotFoundException;
import com.example.luxclusif.google.images.DriveExporter;
import com.example.luxclusif.google.spreadsheet.SheetExporter;
import com.example.luxclusif.model.Case;
import com.example.luxclusif.model.ImageModel;
import com.example.luxclusif.model.bags.Bag;
import com.example.luxclusif.service.BagService;
import com.example.luxclusif.service.CaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
public class RestBagController {

    @Autowired
    private BagService bagService;

    @Autowired
    private CaseService caseService;

    @Autowired
    private BagDtoToBag bagDtoToBag;

    @Autowired
    private ImageHandler imageHandler;

    @Autowired
    private DriveExporter driveExporter;

    @Autowired
    private SheetExporter sheetExporter;

    @PostMapping("/upload/{caseId}")
    public ResponseEntity<String> uploadFiles(@PathVariable Long caseId, @RequestParam("files") MultipartFile[] files) {
        File exportFile;

        for(MultipartFile file : files) {

            if ((exportFile = imageHandler.imageSaver(file, caseId)) == null) {
                System.err.println("Bad request");
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            driveExporter.export(exportFile, caseId);


        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("/addBag")
    public ResponseEntity<Case> addBag(@Valid @RequestBody BagDto bagDto, BindingResult bindingResult) {

        Case aCase = new Case();

        if (bindingResult.hasErrors() || bagDto.getBagId() != null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        try {

            Bag savedBag = bagService.save(bagDto.getCustomerId(), aCase, bagDtoToBag.convert(bagDto));
            System.out.println("Bag id: " + savedBag.getId());
            System.out.println("Customer id: " + savedBag.getaCase().getCustomer().getId());
            System.out.println("After persistence in DB. Case id: " + savedBag.getaCase().getId());

            aCase = caseService.getCase(savedBag.getaCase().getId());

            if (savedBag.getExtras() != null) {
                bagService.updateBag(savedBag.getId(), savedBag.getExtras().getId());
            } else {
                bagService.updateBagWithExtrasNull(savedBag.getId());
            }

            caseService.updateCase(aCase.getId(), savedBag.getId());


            //Take close attention to Google API Sheets and drive
            //Need to work on updating extras in Sheets when extras = 0, there is a bug
            sheetExporter.export("bag", 0L);
            sheetExporter.export("extras", 0L);
            sheetExporter.export("case_tbl", 0L);


        } catch(CustomerNotFoundException e) {

            System.out.println(e.getMessage());
        } catch(IOException e) {

            System.out.println(e.getMessage());
        } catch (GeneralSecurityException e) {

            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println("Exports to Google Sheets failed");
        }

        return new ResponseEntity<>(aCase, HttpStatus.CREATED);
    }

    @GetMapping(value = "/testImageSearch", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<String> testImage() {
        driveExporter.searchImage();

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/testImageDownload")
    public ResponseEntity<String> testImageDownload() {
        driveExporter.testImageDownloaderGdrive();

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(path = "/download")
    public ResponseEntity<ImageModel> download() throws IOException {

        File file = new File("/home/joaocoelho/Desktop/LuxExclusif_exc/LuxExclusif_backend/uploads/Farfetch/resources/bag_images/7/winky-lux-bag-winky-lux-make-up-bag-29415027477_2000x.jpg");
        Path path = file.toPath();
        System.out.println(file.getName());
        System.out.println(file.getAbsolutePath());
        System.out.println(path.toString());

        ImageModel img = new ImageModel();

        img.setId(1L);
        img.setName(file.getName());
        img.setType("image/jpeg");


        byte[] picByte = Files.readAllBytes(path);
        img.setPicByte(picByte);
        img.setLength(picByte.length);

        return new ResponseEntity<>(img, HttpStatus.OK);
    }

    @GetMapping(path = "/downloada")
    public ResponseEntity<Resource> downloadA() throws IOException {

        File file = new File("/home/joaocoelho/Desktop/LuxExclusif_exc/LuxExclusif_backend/uploads/Farfetch/resources/bag_images/7/winky-lux-bag-winky-lux-make-up-bag-29415027477_2000x.jpg");
        Path path = file.toPath();

        System.out.println(path.toString());

        byte[] data = Files.readAllBytes(path);


        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(MediaType.IMAGE_JPEG_VALUE))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment: filename=\"" + file.getName())
                .body(new ByteArrayResource(data));
    }

    @GetMapping(path = "/downloadb")
    public ResponseEntity<byte[]> downloadB() throws IOException {

        File file = new File("/home/joaocoelho/Desktop/LuxExclusif_exc/LuxExclusif_backend/uploads/Farfetch/resources/bag_images/7/winky-lux-bag-winky-lux-make-up-bag-29415027477_2000x.jpg");
        Path path = file.toPath();

        byte[] picByte = Files.readAllBytes(path);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(MediaType.IMAGE_JPEG_VALUE))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment: filename=\"" + file.getName())
                .body(picByte);
    }

}
