package com.example.luxclusif.controller;

import com.example.luxclusif.command.CustomerDto;
import com.example.luxclusif.command.EmailDto;
import com.example.luxclusif.converters.CustomerDtoToCustomer;
import com.example.luxclusif.converters.CustomerToCustomerDto;
import com.example.luxclusif.google.spreadsheet.SheetExporter;
import com.example.luxclusif.model.Customer;
import com.example.luxclusif.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/customer")
public class RestCustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerDtoToCustomer customerDtoToCustomer;

    @Autowired
    private CustomerToCustomerDto customerToCustomerDto;

    @Autowired
    private SheetExporter sheetExporter;

    @PostMapping(path = {"/", ""})
    public ResponseEntity<?> addCustomer(@Valid @RequestBody CustomerDto customerDto, BindingResult bindingResult) {

        Customer customer;

        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (customerService.getByEmail(customerDto.getEmail()) == null) {

            customer = customerService.save(customerDtoToCustomer.convert(customerDto));

            try {
                sheetExporter.export("customer", 0L);
            } catch (Exception e) {
                System.out.println("Can't save customer in Google Sheets. \n" + e.getMessage());
            }

            return new ResponseEntity<>(customerToCustomerDto.convert(customer), HttpStatus.CREATED);
        }


        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PostMapping(path = "/login")
    public ResponseEntity<?> loginCustomer(@RequestBody EmailDto emailDto) {

        String email = emailDto.getEmail();
        Customer customer = customerService.getByEmail(email);

        if (customer == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(customerToCustomerDto.convert(customer), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<CustomerDto> showCustomer(@PathVariable Long id) {

        Customer customer = customerService.get(id);

        if (customer == null) {

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(customerToCustomerDto.convert(customer), HttpStatus.OK);
    }

}
