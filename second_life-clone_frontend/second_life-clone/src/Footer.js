import React from 'react';
import "./Footer.css";

function Footer() {
    return (
        <div className="footer section">
            <div className="container">
                <div className="row">
                    
                    <div className="baseline col-lg-4 col-md-12 col-sm-12 col-xs-12 pb30">
                        <div className="accordion-header">
                            <h5>
                                <span className="fas fa-plus show-icon joao1-style"></span>
                                Farfetch App
                            </h5>
                            <div className="accordion-content">
                                <div className="pt10">
                                    <a href="/" className="no-underline color-white primary" target="_blank">
                                        <img src="https://secondlife.farfetch.com/img/tablet.png" alt="tablet-mobile" className="pr20"/>
                                        <span className="discover-app-text color-white block">Farfetch App for iOS and Android</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div className="baseline col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <div className="row pb30" id="country-region-1">
                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h5>
                                    Country/Region, Currency and Language
                                </h5>
                                <div>
                                    <a href="/" className="no-underline color-white primary pl10" target="_blank">
                                        <div className="ChangeCountry_Flag flags flag-gb f18"></div>
                                        <span className="countryregion color-white block pl10">United Kingdom, GBP £</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="row pb30">
                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div className="accordion-header">
                                    <h5>
                                        <span className="fas fa-plus show-icon joao1-style"></span>
                                            Follow Us
                                    </h5>
                                    <div className="accordion-content">
                                        <div className="social-icons pt10">
                                            <div className="inlineBlock">
                                                <a href="/" target="_blank">
                                                    <img src="https://secondlife.farfetch.com/img/social-media-icons/instagram.png" alt="instagram" width="17" height="17"/>
                                                </a>
                                            </div>
                                            <div className="inlineBlock">
                                                <a href="https://www.facebook.com/farfetch.asiapac/?brand_redir=88573992939">
                                                    <img src="https://secondlife.farfetch.com/img/social-media-icons/facebook.png" alt="facebook" width="17" height="17"/>
                                                </a>
                                            </div>
                                            <div className="inlineBlock">
                                                <a href="https://www.twitter.com/farfetch">
                                                <img src="https://secondlife.farfetch.com/img/social-media-icons/twitter.png" alt="twitter" width="17" height="17"/>
                                                </a>
                                            </div>
                                            <div className="inlineBlock">
                                                <a href="https://www.snapchat.com/add/farfetch.com">
                                                    <img src="https://secondlife.farfetch.com/img/social-media-icons/snapchat.png" alt="snapchat" width="17" height="17"/>
                                                </a>
                                            </div>
                                            <div className="inlineBlock">
                                                <a href="https://www.pinterest.com/farfetch">
                                                    <img src="https://secondlife.farfetch.com/img/social-media-icons/pinterest.png" alt="pinterest" width="17" height="17"/>
                                                </a>
                                            </div>
                                            <div className="inlineBlock">
                                                <a href="https://www.youtube.com/farfetch">
                                                    <img src="https://secondlife.farfetch.com/img/social-media-icons/youtube.png" alt="youtube" width="22" height="17"/>
                                                </a>
                                            </div>
                                            <div className="inlineBlock">
                                                <a href="https://plus.google.com/109435701726892308651?prsrc=3">
                                                <img src="https://secondlife.farfetch.com/img/social-media-icons/google-plus.png" alt="google-plus" width="26" height="17"/>
                                                </a>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            
                    <div className="baseline col-lg-2 col-md-12 col-sm-12 col-xs-12 pb30">
                        <div className="accordion-header">
                            <h5>
                                <span className="fas fa-plus show-icon joao1-style"></span>
                                    Customer Service
                            </h5>
                            <div className="accordion-content">
                                <div className="pt10">
                                    <ul className="list-regular color-white no-underline">
                                        <li>
                                            <a className="no-underline color-white" href="/" target="_blank">Help</a>
                                        </li>
                                        <li>
                                            <a className="no-underline color-white" href="/">Contact Us</a>
                                        </li>
                                        <li>
                                            <a className="no-underline color-white" href="/" target="_blank">FAQs</a>
                                        </li>
                                        <li>
                                            <a className="no-underline color-white" href="/" target="_blank">Terms &amp; Conditions</a>
                                        </li>
                                        <li>
                                            <a className="no-underline color-white" href="/" target="_blank">Privacy Policy</a>
                                        </li>
                                        <li>
                                            <a className="no-underline color-white" href="/" target="_blank" data-toggle="modal" data-target="#cookiesModal">Cookie Preferences</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="baseline col-lg-2 col-md-12 col-sm-12 col-xs-12">
                        <div className="accordion-header">
                            <h5>
                                <span className="fas fa-plus show-icon joao1-style"></span>
                                About Farfetch
                            </h5>
                            <div className="accordion-content">
                                <div className="pt10">
                                    <ul className="list-regular color-white no-underline">
                                        <li>
                                            <a className="no-underline color-white" target="_blank" href="/">About Us</a>
                                        </li>
                                        <li>
                                            <a className="no-underline color-white" target="_blank" href="/">Investors</a>
                                        </li>
                                        <li>
                                            <a className="no-underline color-white" target="_blank" href="/">Farfetch&nbsp;Boutique&nbsp;Partners</a>
                                        </li>
                                        <li>
                                            <a className="no-underline color-white" target="_blank" href="/">Affiliate Programme</a>
                                        </li>
                                        <li>
                                            <a className="no-underline color-white" target="_blank" href="/">Careers</a>
                                        </li>
                                        <li>
                                            <a className="no-underline color-white" target="_blank" href="/">Farfetch&nbsp;Customer&nbsp;Promise</a>
                                        </li>
                                        <li>
                                            <a className="no-underline color-white" target="_blank" href="/">Farfetch App</a>
                                        </li>
                                        <li>
                                            <a className="no-underline color-white" target="_blank" href="/">Farfetch Reviews</a>
                                        </li>
                                        <li>
                                            <a className="no-underline color-white" target="_blank" href="/">Sitemap</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row pt60">
                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 baseline h6">
                        <span className="footer-disclaimer color-medium-grey">
                            'farfetch' and the 'farfetch' logo are trade marks of Farfetch UK Limited and are registered in numerous jurisdictions around the world.
                        </span>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 baseline h6">
                        <span className="footer-disclaimer color-medium-grey">
                            © Copyright 2019 Farfetch UK Limited. All rights reserved.
                        </span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer
