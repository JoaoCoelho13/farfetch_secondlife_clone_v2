import React from 'react';
import "./Header.css";
import { Link } from "react-router-dom";

function Header() {
    return (
        <div className="ff-header d-flex justify-content-center">
            <Link to="/">
                <div className="home home-big d-none d-sm-flex">
                    <img 
                        className="ff-logo align-self-center" 
                        alt="Farfetch Secondlife"
                        src="https://secondlife.farfetch.xom/img/logo/logo-horizontal.svg"
                    />
                </div>
            </Link>
            

            <ul className="ff-header-help d-flex align-items-center">
                <li className="d-none d-md-block">
                    <a href="/">NEED HELP?</a>
                </li>
                <li>
                    
                    <a title="Farfetch Customer Service" href="tel: +351 925344934">
                        <img alt="tel-icon"
                            className="img__phone" 
                            src="https://secondlife.farfetch.com/img/phone.svg"
                        />

                    </a>
                </li>
            </ul>
            
        </div>
    )
}

export default Header
