import React, { Component } from 'react';
import "./SellingPage.css";
import axios from "axios";
import ConfirmationPage from "./ConfirmationPage";

class Sell extends Component {

    constructor(props) {
        super(props)

        this.state = {
            brand: "",
            condition: "",
            size: "",
            extrasBoxBox: 0,
            extrasAuthenticityCard: 0,
            extrasShoulderStrap: 0,
            selectedFile: [],
            transactionMade: false
        }

        this.submitHandler = this.submitHandler.bind(this);
    }

    changeHandler = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    fileSelectedHandler = event => {
        this.setState({
            selectedFile: event.target.files
        })
    }

    submitHandler = e => {
        e.preventDefault();

        axios.post('http://localhost:9191/addBag', {
            condition: this.state.currentCondition,
            brand: this.state.brand,
            size: this.state.size,
            extras: {
                box: this.state.extrasBoxBox,
                shoulderStrap: this.state.extrasShoulderStrap,
                authenticityCard: this.state.extrasAuthenticityCard
            },
            customerId: this.props.userIdInfo
        })
            .then(response => {
                console.log(response.data.id);

                const fd = new FormData();

                for (let i = 0; i < this.state.selectedFile.length; i++) {
                    fd.append('files', this.state.selectedFile.item(i));
                }

                axios.post('http://localhost:9191/upload/' + response.data.id, fd)
                    .then(res => {
                        console.log(res);
                        this.setState({
                            transactionMade : true
                        })
                        
                    })
                    .catch(error => {
                        console.log(error);
                    })
            })
            .catch(error => {
                console.log(error)
            })

    }

    testingImage = function(e) {
        e.preventDefault();
    
        axios.get('http://localhost:9191/testImageDownload')
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                console.log(error);
                alert("error occured.");
            }) 
    }

    render() {
        const { transactionMade, currentCondition, brand, size } = this.state;


        var stateToSend = this.props.userIdInfo;

        console.log(this.state);

        return (
            <div>

            {
                !transactionMade? (
                    <div>
                <section>
                    <div className="heromask joao1-style" id="#top">
                        <div className="banner-text">Start Selling</div>
                    </div>
                    <div id="ff-sell-form-x22" className="container">
                        <form
                            id="contact_form"
                            className="form-horizontal"
                            onSubmit={this.submitHandler}>
                            <fieldset>
                                <div className="row-1">
                                    <div className="col-lg-12">
                                        <div className="center">
                                            <p className="joao2-style">
                                                Tell us about your bag and we will tell you how much credit you could earn.
                                            </p>
                                            <p className="joao-3-style">
                                                Before submitting your application please
                                                <a className="p-guide" href="/" data-toggle="modal" data-target="#photoGuidelineModal">review our photo guidelines</a> and note that this service is currently only available to customers in the UK and select European Union Countries. Please see FAQs for more details.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div className="instruction">
                                    <div id="userForm" className="row">
                                        
                                        <div className="col-lg-6 ff-section">
                                            <label className="ff-title polar-bold"
                                                htmlFor="currentCondition">Condition</label>
                                            <select
                                                id="currentCondition"
                                                className="form-control text-form p-2"
                                                type="text"
                                                name="currentCondition"
                                                value={currentCondition}
                                                required
                                                onChange={this.changeHandler}
                                            >
                                                <option value="">Select condition</option>
                                                <option value="Pristine">Pristine</option>
                                                <option value="Excellent">Excellent</option>
                                                <option value="Very Good">Very Good</option>
                                                <option value="Good">Good</option>
                                                <option value="Used">Used</option>
                                                <option value="Poor">Poor</option>
                                            </select>
                                        </div>
                                        <div className="col-lg-6 ff-section">
                                            <label className="ff-title polar-bold"
                                                htmlFor="brand">Brand</label>
                                            <select
                                                id="brand"
                                                className="form-control text-form p-2"
                                                type="text"
                                                name="brand"
                                                value={brand}
                                                required
                                                onChange={this.changeHandler}
                                            >
                                                <option
                                                value=""
                                                >Select Brand</option>
                                                <option value="Chanel">Chanel</option>
                                                <option value="Dior">Dior</option>
                                                <option value="Dolce&Gabbana">Dolce&Gabbana</option>
                                                <option value="Givenchy">Givenchy</option>
                                            </select>
                                        </div>
                                        <div className="col-lg-6 ff-section">
                                            <label className="ff-title polar-bold"
                                                htmlFor="size">Size
                                                <span>Please select whichever you think is most representative of your bag</span>
                                            </label>
                                            <select
                                                id="size"
                                                className="form-control text-form p-2"
                                                type="text"
                                                name="size"
                                                value={size}
                                                required
                                                onChange={this.changeHandler}
                                            >
                                                <option value="">Select size - (XS,S,M,L,XL)</option>
                                                <option value="XS"> Extra Small </option>
                                                <option value="SMALL"> Small </option>
                                                <option value="MEDIUM"> Medium </option>
                                                <option value="LARGE"> Large </option>
                                                <option value="TRAVEL"> Extra Large </option>
                                            </select>
                                        </div>
                                        <div className="col-lg-6 ff-section">
                                            <label className="ff-title polar-bold"
                                                htmlFor="extras">
                                        <span>Any extras?
                                        </span>
                                        Please indicate any original accessories that you are able to include below
                                            </label>
                                            <div className="spacing-options">
                                            <label>Box
                                                <input
                                                id="extrasBoxBox"
                                                className="form-control text-form p-2"
                                                type="checkbox"
                                                name="extrasBoxBox"
                                                value="1"
                                                
                                                onChange={this.changeHandler}
                                                />
                                            </label>
                                            </div>
                                            <div className="spacing-options">
                                            <label>Authenticity Card
                                                <input
                                                id="extrasAuthenticityCard"
                                                className="form-control text-form p-2"
                                                type="checkbox"
                                                name="extrasAuthenticityCard"
                                                value="1"
                                                
                                                onChange={this.changeHandler}
                                                />
                                            </label>
                                            </div>
                                            <div className="spacing-options">
                                            <label>Shoulder Strap
                                                <input
                                                id="extrasShoulderStrap"
                                                className="form-control text-form p-2"
                                                type="checkbox"
                                                name="extrasShoulderStrap"
                                                value="1"
                                                
                                                onChange={this.changeHandler}
                                                />
                                            </label>
                                            </div>
                                            
                                        </div>
                                        <label className="ff-title polar-bold"
                                            htmlFor="images">Upload at least 3 images of your product 
                                            
                                            </label>
                                            <h1> </h1>
                                        <div>
                                            <input
                                                id="images"
                                                type="file"
                                                placeholder="Upload Images"
                                                multiple
                                                onChange={this.fileSelectedHandler}
                                                />
                                        </div>
                                    </div>
                                    <div>
                                        <button type="submit">Submit</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                        
                        <button onClick={this.testingImage}>Click to test</button>
                    </div>
                   
                </section>
            </div>
            ) : (
            <ConfirmationPage user={stateToSend}/>
            )}
            </div>
            
        )
    }

}

export default Sell;
