import React from 'react';
import "./LandingPage.css";
import { Link } from "react-router-dom";



function LandingPage() {

    return (
        <div className="section">
            <div className="heromask2">
                <div className="d-flex p-2 my-3 justify-content-center we-are-here joao-style">
                    <p className="m-0">
                        <span className="font-p-bold">We are here for you</span>
                        <span className="px-1">|</span>
                    Farfetch Second Life is continuing to pick up your items safely from home
                </p>
                </div>
                <picture>
                    <source type="image/webp"
                        srcSet="https://secondlife.farfetch.com/img/banners/ff-desktop.webp"
                    />
                    <source type="image/jpeg"
                        srcSet="https://secondlife.farfetch.com/img/banners/ff-desktop.jpg"
                    />
                    <img className=" " src="https://secondlife.farfetch.com/img/banners/ff-desktop.jpg"
                        alt="September Banner"
                    />
                </picture>
            </div>

            <div id="welcome-sell" className="container ff-section ff-section-top joao2-style">
                <div className="row">
                    <div className="col-lg-12">
                        <p className="intro ff-title">
                            Sell your designer bags for Farfetch credit
                        </p>
                        <p className="joao3-style">
                            Introducing Farfetch Second Life, a new service to enable you to make space in your wardrobe by selling your bags
                        <br></br>
                        in exchange for credits to spend on Farfetch. Give your bags a second lease of life. Earn credit.*
                        </p>
                        <div className="joao4-style">
                            *Available in the UK, European Union and Norway. Please see
                        <a href="/">FAQs</a>
                        for more details.
                        <br></br>
                        Based elsewhere?
                        <a href="/">Get notified</a>
                        when Second Life launches near you.
                        </div>
                    </div>
                </div>

                <div className="col-lg-12 ff-title p-0 mt-4">
                    <Link to="/login">
                        <button id="ff-selling-up" className="btn btn-black ff-cta">
                        Start selling
                    </button>
                    </Link>
                </div>
            </div>

            <div className="container ff-section">
                <div className="row ff-title">
                    <div className="col-lg-12">
                        <p className="howitworks">
                            <strong>Here's how it works...</strong>
                        </p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-4">
                        <center className="ff-title">
                            <img alt="how-it-works-1"
                                src="https://secondlife.farfetch.com/img/how-it-works/1.png"
                                className="joao5-style" />
                        </center>
                        <p className="steps ff-title">Get your quote</p>
                        <center>
                            <p className="substeps1 ff-title">
                                Upload photos of the bag you want to sell from the brands listed below, and within 2 business days we'll tell you how much credit you could earn
                            </p>
                        </center>
                    </div>
                    <br></br>
                    <div className="col-lg-4">
                        <center className="ff-title">
                            <img alt="how-it-works-2" src="https://secondlife.farfetch.com/img/how-it-works/2.png" className="joao5-style"></img>
                        </center>
                        <p className="steps ff-title">Arrange a free collection</p>
                        <center>
                            <p className="substeps2 ff-title">
                                Schedule a pick-up at a time that suits you, then we’ll take care of the rest
                            </p>
                        </center>
                    </div>
                    <div className="col-lg-4">
                        <center className="ff-title">
                            <img alt="how-it-works-3" src="https://secondlife.farfetch.com/img/how-it-works/3.png"
                                className="joao5-style" />
                        </center>
                        <p className="steps ff-title">Earn Farfetch credit</p>
                        <center>
                            <p className="substeps3 ff-title">
                                Get credit in just two business days once your bag has been received and verified at our warehouse
                            </p>
                        </center>
                    </div>
                </div>
            </div>

            <section className="ff-section whats-section">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6 col-md-6">
                            <div className="row-1 ff-title">
                                <div id="img-div" className="mask">
                                    <div className="bg-webp-img has-no-webp"></div>
                                    <center>
                                        <p className="whatsin joao6-style">
                                            What's in it for me?
                                        </p>
                                    </center>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-6">
                            <div className="row-1 newstuff">
                                <div className="card joao7-style">
                                    <p className="steps ff-title">Instant payment</p>
                                    <center>
                                        <p className="substeps1 ff-section">
                                            We give you credit as soon as your bag has been<br className="d-none d-lg-block"></br>
                                                verified so you can fund your next purchase faster
                                        </p>
                                    </center>
                                    <p className="steps ff-title">Get something new
                                    </p>
                                    <center>
                                        <p className="substeps1 ff-section">
                                            Use your credit to buy new things on Farfetch
                                        </p>
                                    </center>
                                    <p className="steps ff-title">No fees</p>
                                    <center>
                                        <p className="substeps1 ff-section">
                                            The price we give you is the value of the credit you will get.<br className="d-none d-lg-block"></br>
                                            No hidden fees.
                                        </p>
                                    </center>
                                    <p className="steps ff-title">Do good</p>
                                    <center>
                                        <p className="substeps1 ff-section">
                                            By selling your bag, you’re doing your bit to extend<br className="d-none d-lg-block"></br>
                                            its life and help the environment
                                        </p>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <div className="container">
                <div className="row-1 ff-title">
                    <center><p className="welcome">At the moment we accept the following brands</p></center>
                </div>
            </div>

            <div className="container ff-title brands-container joao8-style">
                <div className="row">
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <p className="brands hd-sml">Alexander McQueen</p>
                        <p className="brands hd-lrg">A.McQueen</p>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <p className="brands hd-sml">Alexander Wang</p>
                        <p className="brands hd-lrg">A.Wang</p>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Balenciaga</p>
                        </center>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Bottega Veneta</p>
                        </center>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Burberry</p>
                        </center>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Bvlgari</p>
                        </center>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Cartier</p>
                        </center>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Celine</p>
                        </center>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Chanel</p>
                        </center>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Dior</p>
                        </center>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Dolce &amp; Gabbana</p>
                        </center>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Fendi</p>
                        </center>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Givenchy</p>
                        </center>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Gucci</p>
                        </center>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Hermès</p>
                        </center>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Jacquemus</p>
                        </center>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Loewe</p>
                        </center>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Louis Vuitton</p>
                        </center>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> MCM</p>
                        </center>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Miu Miu</p>
                        </center>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Mulberry</p>
                        </center>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Off White</p>
                        </center>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Prada</p>
                        </center>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Salvatore Ferragamo</p>
                        </center>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Stella McCartney</p>
                        </center>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> The Row</p>
                        </center>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Valentino</p>
                        </center>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> Versace</p>
                        </center>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p className="brands"> YSL</p>
                        </center>
                    </div>
                </div>
                <p className="text-center joao9-style">Watch this space though, we're working on a broader initiative to enable you  to trade in other items
                </p>
            </div>

            <div className="container ff-section">
                <div className="center">
                    <p className="ff-h1">
                        See what previous customers have sold
                    </p>
                </div>

                <div className="row ff-customer-bags">
                    <div className="col p-2">
    
                        <img alt="AltText" 
                        src="https://secondlife.farfetch.com/img/pricing_skus/GUCCI.png" 
                        srcSet="https://secondlife.farfetch.com/img/pricing_skus/GUCCI-small.png 390w, 
                        https://secondlife.farfetch.com/img/pricing_skus/GUCCI.png 400w" className="img-responsive"/>
                        <p className="ff-cb-brand mt-3">Gucci</p>
                        <p className="ff-cb-name">Large Leather Soho Tote</p>
                        <p className="ff-cb-price mt-1">£265</p>
                        <p className="ff-rating">Condition: Good</p>
                    </div>

                    <div className="col p-2">
                        <img alt="AltText" src="https://secondlife.farfetch.com/img/pricing_skus/LV.png" srcSet="https://secondlife.farfetch.com/img/pricing_skus/LV-small.png 390w, 
                        https://secondlife.farfetch.com/img/pricing_skus/LV.png 400w" className="img-responsive" loading="lazy"/>
                        <p className="ff-cb-brand mt-3">Louis Vuitton</p>
                        <p className="ff-cb-name">Damier Ebene Trevi PM</p>
                        <p className="ff-cb-price mt-1">£485</p>
                        <p className="ff-rating">Condition: Good</p>
                    </div>

                    <div className="col p-2">
                        <img alt="AltText" src="https://secondlife.farfetch.com/img/pricing_skus/CHLOE.png" srcSet="https://secondlife.farfetch.com/img/pricing_skus/CHLOE-small.png 390w, 
                        https://secondlife.farfetch.com/img/pricing_skus/CHLOE.png 400w" className="img-responsive" loading="lazy"/>
                        <p className="ff-cb-brand mt-3">Chloe</p>
                        <p className="ff-cb-name">Medium Leather Aby</p>
                        <p className="ff-cb-price mt-1">£805</p>
                        <p className="ff-rating">Condition: Excellent</p>
                    </div>


                    <div className="col p-2">
                        <img alt="AltText" src="https://secondlife.farfetch.com/img/pricing_skus/DIOR.png" srcSet="https://secondlife.farfetch.com/img/pricing_skus/DIOR-small.png 390w, 
                        https://secondlife.farfetch.com/img/pricing_skus/DIOR.png 400w" className="img-responsive" loading="lazy"/>
                        <p className="ff-cb-brand mt-3">Dior</p>
                        <p className="ff-cb-name">Embroidered Book Tote</p>
                        <p className="ff-cb-price mt-1">£1,440</p>
                        <p className="ff-rating">Condition: Pristine</p>
                    </div>
                </div>
            </div>

            <div className="container joao10-style">
                <center>
                    <p className="ff-section joao11-style">
                        Please note that the price that you will get for your bag will depend on its condition - the better the condition, the better the price.
                        <br></br>
                        We only accept items that meet our condition criteria
                    </p>
                </center>
                <br></br>
            </div>

            <div className="container ff-section joao8-style">
                <center>
                    <p className="ff-title joao12-style">Got a bag to sell?</p>
                    <div className="col-lg-12 ff-title p-0">
                        <Link to="/login">
                            <button id="ff-selling-started" className="btn btn-black ff-cta">
                            Start selling
                            </button>
                        </Link>
                        
                    </div>
                </center>
            </div>

            <div className="container ff-section joao13-style">
                <div className="center">
                    <p className="ff-h1">What our customers say about their Second Life experience...</p>
                
                    <div className="row pt-3">
                        <div className="col-md-6 col-sm-12 p-2">
                            <p className="font-italic">
                                <span className="s2-bold">“</span> I think the overall process was 
                                <span className="polaris-bold">super easy, simple and well-laid out</span>.
                                <span className="s2-bold">”</span>
                            </p>
                            <p className="p-author">by Manal W.</p>
                        </div>
                        <div className="col-md-6 col-sm-12 p-2">
                            <p className="font-italic">
                                <span className="s2-bold">“</span> Everything went smoothly as explained in the mail. <span className="polaris-bold">So easy, so fast.</span> Thank you!<span className="s2-bold"> ”</span>
                            </p>
                            <p className="p-author">by Deniz G.</p>
                        </div>
                        <div className="col-md-6 col-sm-12 offset-md-3 p-2">
                            <p className="font-italic">
                                <span className="s2-bold">“</span> The service was <span className="polaris-bold">informative and efficient</span>. I will definitely be using this service and recommending to friends and family <span className="s2-bold">”</span>
                            </p>
                            <p className="p-author">by Iram R.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div className="container ff-section joao8-style">
                <center>
                    <p className="joao14-style">
                        <br></br>
                        We're on a mission to become the global platform for good in luxury fashion – empowering everyone to think, act and choose positively. Services like Farfetch Second Life help our customers extend the life of the clothes they buy. It's all part of what we call Positively Farfetch.
                    </p>
                    <p className="joao15-style">
                        <br></br>
                        <a href="/" title="Positively Farfetch">
                            <img src="https://secondlife.farfetch.com/img/logo/logo-vertical-positively.svg" alt="Positively Farfetch" className="positively img-fluid"/>
                        </a>
                    </p>
                </center>
            </div>

        
        </div>

    )
}

export default LandingPage;
