import React, { Component } from 'react';
import "./LoginPage.css";
import axios from "axios";
import SellingPage from "./SellingPage";


class LoginPage extends Component {

    constructor(props) {
        super(props)

        this.state = {
            firstName: "",
            lastName: "",
            signupEmail: "",
            signinEmail: "",
            phone: "",
            userId: 0,
            userAuth: false
        }

        this.createAccountHandler = this.createAccountHandler.bind(this);
        this.loginHandler = this.loginHandler.bind(this);
    }

    changeHandler = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    createAccountHandler = e => {
        e.preventDefault();

        axios.post('http://localhost:9191/api/customer/', {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.signupEmail,
            phone: this.state.phone
        })
            .then(response => {
                console.log(response);
                alert("Customer created successfully");

            })
            .catch(error => {
                console.log(error);

                alert("Unable to create Customer, please try again")
            })

    }

    loginHandler = e => {
        e.preventDefault();

        axios.post('http://localhost:9191/api/customer/login', {
            email: this.state.signinEmail
        })
            .then(response => {
                console.log(response.data.id);

                this.setState({
                    userId : response.data.id,
                    userAuth : true
                })
                
            })
            .catch(error => {
                console.log(error);
                alert("That user does not exist. Please Sign Up first.")
            })

    }


    render() {
        const { firstName, lastName, signupEmail, signinEmail, phone, userAuth, userId } = this.state;

        return (
            <div>
                {
                    userAuth ? (
                        <div>
                            <SellingPage userIdInfo={userId}/>
                        </div>
                    ) : (
                            <div>
                                <div className="container" id="container">
                                    <div className="form-container sign-up-container">
                                        <form id="createAccountForm"
                                            onSubmit={this.createAccountHandler}>
                                            <h1>Create Account</h1>

                                            <input id="firstName"
                                                type="text"
                                                name="firstName"
                                                placeholder="firstName"
                                                required
                                                value={firstName}
                                                onChange={this.changeHandler}
                                            />
                                            <input id="lastName"
                                                name="lastName"
                                                type="text"
                                                placeholder="Lastname"
                                                required
                                                value={lastName}
                                                onChange={this.changeHandler}
                                            />
                                            <input id="signupEmail"
                                                name="signupEmail"
                                                type="email"
                                                placeholder="Email"
                                                required
                                                value={signupEmail}
                                                onChange={this.changeHandler}
                                            />
                                            <input id="phone"
                                                name="phone"
                                                type="text"
                                                placeholder="Phone"
                                                required
                                                value={phone}
                                                onChange={this.changeHandler}
                                            />
                                            <button id="createAccountButton"
                                                type="submit">Sign Up</button>

                                        </form>
                                    </div>
                                    <div className="form-container sign-in-container">
                                        <form id="login" onSubmit={this.loginHandler}>
                                            <h1>Sign in</h1>
                                            <input id="signinEmail"
                                                name="signinEmail"
                                                type="email"
                                                placeholder="Email"
                                                required
                                                value={signinEmail}
                                                onChange={this.changeHandler}
                                            />
                                            <button id="loginButton" type="submit">Sign In</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        )
                }
            </div>
        )
    }
}

export default LoginPage
