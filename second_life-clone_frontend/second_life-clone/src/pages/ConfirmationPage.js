import React, {Component} from 'react';
import "./ConfirmationPage.css";
import { Link } from "react-router-dom";
import axios from 'axios';

class ConfirmationPage extends Component {

    constructor(props) {
        super(props)

        
        this.state = {
            customerId : this.props.user,
            userDetailsExist: false,
            email: "",
            firstName: "",
            lastName: "",
            phone: "",
            balance: ""
        }

        this.userDetailsSubmitHandler = this.userDetailsSubmitHandler.bind(this);
    }

    userDetailsSubmitHandler = e => {
        e.preventDefault();

        axios.get('http://localhost:9191/api/customer/' + this.props.user)
            .then(response => {
                console.log(response)

                this.setState({
                    userDetailsExist : true,
                    email: response.data.email,
                    balance: response.data.balance,
                    firstName: response.data.firstName,
                    lastName: response.data.lastName,
                    phone: response.data.phone
                })
            })
            .catch(error => {
                console.log(error)
            })
    }



    render() {

        const {userDetailsExist, customerId, email, balance, firstName, lastName, phone} = this.state;
        

        return (
            <div>
                <h4>Congratulations your transaction has been completed successfully!
                </h4>

                <form id="userDetails"
                onSubmit={this.userDetailsSubmitHandler}>

                    <label htmlFor="userId">User ID:</label>
                    <input id="userId" type="text" value={this.props.user}
                    placeholder={this.props.user}
                    readOnly>

                    </input>
                    <button id="userDetailsButton" type="submit">Get User Details</button>
                </form>

                {userDetailsExist? 
                
                    (
                        <div>
                            <p>User id : {customerId}</p>
                            <p>First Name : {firstName}</p>
                            <p>Last Name : {lastName}</p>
                            <p>Email : {email}</p>
                            <p>Phone : {phone}</p>
                            <p>Balance : {balance}</p>
                        </div>
                    
                    ) : (
                        <div> </div>
                    )
                    
                }




                <p>
                    Continue Selling?
                </p>
                
                <div>
                    <Link to="/">
                        <button>Back to Homepage</button>
                    </Link>
                </div>

            </div>
        )
    }
}

export default ConfirmationPage
