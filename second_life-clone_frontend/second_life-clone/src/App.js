import React from 'react';
import './App.css';
import Header from "./Header";
import Footer from "./Footer";

import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";

import LandingPage from "./pages/LandingPage";
import LoginPage from "./pages/LoginPage";
import SellingPage from "./pages/SellingPage";

function App() {
  return (

    <Router>
      <div className="app">
        <div className="app__header">
          <Header />
        </div>

        <Switch>
          <Route exact path="/" component={LandingPage} />
          <Route exact path="/login" component={LoginPage} />
          <Route exact path="/sell" component={SellingPage}/>
          <Redirect to="/"/>
        </Switch>
        
        <div className="app_footer">
          <Footer />
        </div>
      </div>
    </Router>

  );
}

export default App;
